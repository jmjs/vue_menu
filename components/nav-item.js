
export default ()=> {
  return `<section v-if="show" class="groupIcon" v-bind:id="label" v-on="event ? { click: (e) => secondary(e) } : {}" >
  <img v-bind:src="svg" alt="ic2" srcset="">
  <label>{{label}}</label>
</section>`
}

//https://www.svgrepo.com/show/54984/down.svg